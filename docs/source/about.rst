À propos
========

K-lity est une application de test automatisés créée au sein de
`Klee Group <https://kleegroup.com/>`_ dans le but d'améliorer la qualité des
applications produites.


Licence
-------

K-lity est publié sous la licence
`AGPL V3+ <http://www.gnu.org/licenses/agpl-3.0.en.html>`_.
