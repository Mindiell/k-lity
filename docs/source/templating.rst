.. _templating:

Utiliser ses propres templates
==============================

Si vous souhaitez utiliser vos propres templates pour la génération du rapport de test,
il vous faut créer la structure adéquate :

::

    your_project
    ├─ steps
    └─ templates
       └─ template_name
          ├─ index.html
          └─ static

Le répertoire *templates* contient tous vos templates personnels. Chacun de ces templates
est représenté par un répertoire distinct. Chaque template contient au moins un fichier
*index.html* qui sera utilisé pour générer le rapport.

Le répertoire *static* est optionnel et permet de stocker des médias qui seront tous
copiés sous la même structure lors de la génération du rapport.


Modèle de template
------------------

Les rapports générés par K-lity s'appuient sur le système de templating
`Jinja2 <https://jinja2docs.readthedocs.io/en/stable/>`_.


Données fournies au template
----------------------------

Les données fournies au template le sont sous la forme d'un dictionnaire aisément
utilisable :

::

    data = {
        "title": string,                    -- Titre du rapport
        "total": integer,                   -- Nombre total de fonctionnalités
        "danger": integer,                  -- Nombre de fonctionnalités ayant plus de 40% de scénarios échoués
        "warning": integer,                 -- Nombre de fonctionnalités ayant au moins un scénario échoué
        "success": integer,                 -- Nombre de fonctionnalités sans scénario échoué
        "passed": integer,                  -- Indique le nombre de fonctionnalités passées
        "failed": integer,                  -- Indique le nombre de fonctionnalités échouées
        "time": integer,                    -- Indique le temps écoulé en secondes
        "time_elapsed": string,             -- Indique le temps écoulé au format HH:MM:SS
        "features": [                       -- Liste des fonctionnalités
            "name": string,                 -- Nom de la fonctionnalité
            "scenarios": [                  -- Liste des scénarios de la fonctionnalité
                {
                    "name": string,         -- Nom du scénario
                    "passed": boolean,      -- Flag indiquant si le scénario est passé ou échoué
                    "logs": [],             -- Liste des lignes de logs du scénario
                },
            ],
            "passed": integer,              -- Indique le nombre de scénarios passés
            "failed": integer,              -- Indique le nombre de scénarios échoues
            "state": string,                -- Indique l'état de la fonctionnalité (danger, warning, success)
            "total": integer,               -- Nombre total de scénarios
            "time": integer,                -- Indique le temps écoulé en secondes
            "time_elapsed": string,         -- Indique le temps écoulé au format HH:MM:SS
        ],
        "generation": time,                 -- Date/heure de génération du rapport
    }
