K-lity
======

**K-lity est un outil permettant d'effectuer des tests web de manière automatisée**.
Pour cela, il repose sur les composants `selenium
<https://www.selenium.dev/documentation/fr/>`_, qui permet de contrôler le navigateur,
et `behave <https://behave.readthedocs.io/>`_, qui permet de lancer des tests par
fonctionnalité.

L'idée principale de K-lity est de **faciliter l'écriture et la lecture des tests**,
permettant ainsi à des personnes non techniques de participer à ceux-ci.

Les tests sont regroupés par *Fonctionnalité*. Une *Fonctionnalité* est un ensemble de
Scénarios qui permettent de valider une fonctionnalité d'un logiciel. Chaque *Scénario*
contient un ensemble d'étapes permettant d'exécuter celui-ci afin de valider son bon
déroulement. Le langage Gherkin est utiliséet le but est de pouvoir écrire ou lire un
*Scénario* en le rendant compréhensible à toute personne, même si celle-ci ne
connait pas l'application.


Démarrage rapide
----------------

:ref:`quick_start`


Installation
------------

:ref:`installation`


Options
-------

Klity se lance en ligne de commande et accepte des options :

Choix des fonctionnalités
~~~~~~~~~~~~~~~~~~~~~~~~~

Afin de limiter les fonctionnalités à tester, il peut être intéressant de filtrer celles-ci. Pour cela, l'option --filename (ou -f) est utilisée. Les mots situés après cette option doivent tous être présents dans le nom global du fichier de la fonctionnalité pour que celle-ci soit prise en compte.
Si on utilise plusieurs fois cette option, plusieurs fonctionnalités peuvent donc être trouvés indépendamment les uns des autres.

Choix des scénarios
~~~~~~~~~~~~~~~~~~~

Afin de limiter les scénarios à exécuter, l'option --name (ou -n) peut être utilisée. Les noms des Scénarios qui correspondent à cette option seront alors les seuls à être exécutés.

Nettoyage des anciens résultats
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Afin d'être sûr que les résultats des tests soient les plus récents, il peut être
nécessaire de les supprimer avant de lancer les tests. L'option --clean permet cela.

Ne pas exécuter les tests
~~~~~~~~~~~~~~~~~~~~~~~~~

Afin de s'assurer par exemple que les filtres des fonctionnalités sont corrects, il est
possible de ne pas exécuter les tests. Il suffit alors d'utiliser l'option --dry-run.

Ne pas produire de rapport
~~~~~~~~~~~~~~~~~~~~~~~~~~

Il est également possible de ne pas produire le rapport en fin de test. Il suffit alors
d'utiliser l'option --no-report.

Spécifier le titre du rapport
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Il est possible de spécifier le titre du rapport en utilisant l'option --title (ou -t).

Spécifier le template du rapport
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Il est possible de spécifier le template du rapport en utilisant l'option --template.
Les templates actuellement disponibles dans k-lity sont : "default", "dotmap" et
"default_with_logs".

Il est possible d'utiliser :ref:`vos propres templates<templating>`.


.. toctree::
   :maxdepth: 2
   :caption: Contents:

   quick.rst
   installation.rst
   contributing.rst
   using/index.rst
   configuration.rst
   templating.rst
   about.rst
