.. _variables:

Les variables
=============

Afin de permettre la dynamisation des tests, il est possible d'utiliser des
variables dans les valeurs données aux étapes.

Une variable est définie par une chaine de caractères au format '$.....$',
où la partie située entre les double dollars correspond au nom de la variable.
