.. _given_steps:

Les étapes de type 'Etant donné'
================================

Se rendre à une url précise
---------------------------

Vocabulaire
~~~~~~~~~~~

.. code-block:: gherkin

    que je visite le site "{url}"
    que je visite l'url "{url}"

Exemples
~~~~~~~~

.. code-block:: gherkin

    Etant donné que je visite le site "https://toto.fr/"
    Etant donné que je visite l'url "https://toto.fr/"

Quand l'utiliser
~~~~~~~~~~~~~~~~

On utilise cette étape lorsqu'on souhaite afficher une page spécifique.
