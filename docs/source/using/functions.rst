.. _functions:

Les fonctions
=============

Afin de permettre la dynamisation des tests, il est possible d'utiliser des
fonctions dans les valeurs données aux étapes.

Une fonction est définie par une chaine de caractères au format '##.......##',
où la partie située entre les double dièses correspond au comportement attendu.

Le comportement correspond à un champ *quoi* et un champ *comment*. Ces deux
champs sont séparés par un caractère "_". Alors que le champ *quoi* désigne ce
qui est attendu, le champ *comment* désigne le format attendu.

Les *quoi* possibles sont :

* JOUR


JOUR
----

Cette fonction correspond à une date. Par défaut, la date du jour est
sélectionnée. Il est cependant possible d'augmenter ou diminuer le jour en
ajoutant une opération.

La fonction supporte les formats suivants :

* JOUR
* MOIS
* ANNEE

Exemples :

::

    ##JOUR_JOUR##

Cette fonction renvoit le jour du jour actuel.

::

    ##JOUR-1_MOIS##

Cette fonction renvoit le mois de la veille du jour actuel.


JOUR
~~~~

Renvoit le jour sur 2 chiffres.

MOIS
~~~~

Renvoit le mois sur 2 chiffres.

ANNEE
~~~~~

Renvoit l'année sur 4 chiffres.
