Utiliser Klity
==============


.. toctree::
   :maxdepth: 2
   :caption: Contents:

   steps.rst
   given_steps.rst
   when_steps.rst
   then_steps.rst
   utility_steps.rst
   variables.rst
   functions.rst
