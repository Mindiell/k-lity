.. _steps:

Les étapes
==========

Chaque étape est composée d'une phrase pouvant comporter une ou plusieurs
variables qui doivent systématiquement être entourée de guillemets (pour des
raisons techniques et de lisibilité).

Les étapes sont rendues sous trois types principaux :

 * :ref:`given_steps` : Ce type d'étape permet de définir un état de départ pour chaque test
 * :ref:`when_steps` : Ce type d'étape permet de définir un enchainement d'actions à réaliser
 * :ref:`then_steps` : Ce type d'étape permet de définir un état de sortie pour chaque test

Et un sous type spécifique :

 * :ref:`utility_steps` : Ces étapes sont utilisables à n'importe quel niveau


Rédiger un test
---------------

Lorsque plusieurs étapes s'enchainent, et pour la simplicité de lecture, il est
possible de remplacer le mot clef par "**Et**". Par exemple :

.. code-block:: gherkin

    Etant donné que je visite le site "https//example.org/"
    Quand je clique sur le bouton "Aide"
    Et que je tape "toto" dans le champ "titi"
    Et que je coche la case à cocher "Monsieur"
    Et que je sélectionne la valeur "Oui" du champ "Acceptez-vous"
    Et que je clique sur le bouton "Appeler"
    Alors la page contient "Bonjour"
    Et la page contient "Comment allez-vous ?"

Enfin, chaque étape peut avoir quelques subtilités à connaitre pour bien
comprendre comment tel ou tel élément de la page est sélectionné. Dans tous les
cas, la sélection recherche au plus large, et une fois la liste des éléments
potentiels faite, elle prend en compte uniquement le premier élement visible.

La sélection des éléments est détaillée pour chaque étape au niveau de la section
"Subtilités de sélection".
