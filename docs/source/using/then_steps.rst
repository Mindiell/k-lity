.. _then_steps:

Les étapes de type 'Alors'
==========================

Les étapes du type 'Alors' sélectionnent des éléments invisibles par défaut.


Vérification du titre de la page
--------------------------------

Vocabulaire
~~~~~~~~~~~

.. code-block:: gherkin

    le titre de la page contient "{valeur}"
    le titre de la page est "{valeur}"

Exemples
~~~~~~~~

.. code-block:: gherkin

    Alors le titre de la page contient "Titre"
    Et le titre de la page est "Accueil"

Quand l'utiliser
~~~~~~~~~~~~~~~~

Cette étape permet de vérifier un titre de page, et donc généralement que la page affichée est celle attendue.


Vérification du contenu de la page
----------------------------------

Vocabulaire
~~~~~~~~~~~

.. code-block:: gherkin

    la page contient "{valeur}"
    la page contient "{valeur}" ou "{autre*valeur}"
    la page ne contient pas "{valeur}"

Exemples
~~~~~~~~

.. code-block:: gherkin

    Alors la page contient "Accueil"
    la page contient "résultats trouvés" ou "Aucun résultat"
    la page ne contient pas "Ré-essayer"

Quand l'utiliser
~~~~~~~~~~~~~~~~

Cette étape permet de vérifier la présence (ou l'absence) de texte dans la page.

Subtilités de sélection
~~~~~~~~~~~~~~~~~~~~~~~

Le texte testé est celui présent entre les tags (et non "dans" les tags), il est donc impossible de "vérifier" le contenu d'un bouton, même si celui-ci semble visible sur la page.

Afin de maximiser la reconnaissances, le texte de la page est complètement dépuillé de tous les tags, puis tous les sauts de ligne et autres multiples espaces sont supprimés.


Vérification du titre ou infobulle d'un élément
-----------------------------------------------

Vocabulaire
~~~~~~~~~~~

.. code-block:: gherkin

    le titre de l'élément "{element}" est "{text}"
    le titre de l'élément "{element}" contient "{text}"
    le titre de l'élément qui contient "{element}" est "{text}"
    le titre du champ "{field}" est "{text}"

Exemples
~~~~~~~~

.. code-block:: gherkin

    Alors le titre de l'élément "Entete" est "Accueil"
    Et le titre de l'élément "Entete" contient "Acc"
    Et le titre de l'élément qui contient "Ceci est un message" est "Message à caractère informatif"
    Et le titre du champ "Titre" est "Titre de la page"

Quand l'utiliser
~~~~~~~~~~~~~~~~

Cette étape permet de vérifier la valeur *title* d'un infobulle.

Subtilités de sélection
~~~~~~~~~~~~~~~~~~~~~~~

S'agissant de l'élément :

1. En premier lieu, tous les éléments sauf **p** et **div** dont le texte contenu vaut *valeur* sont sélectionnés.
2. Si aucun élément n'est trouvé, une nouvelle recherche se fait en se basant sur l'**id** de n'importe quel élément.
3. Si aucun élément n'est trouvé, une nouvelle recherche se fait en se basant sur le **name** de n'importe quel élément.
4. Si aucun élément n'est trouvé, une nouvelle recherche se fait en se basant sur le **value** de n'importe quel élément.
5. Si aucun élément n'est trouvé, une nouvelle recherche se fait en se basant sur le texte contenu de n'importe quel élément.

S'agissant du champ :

1. En premier lieu, la recherche se fait pour les tags **label** dont le contenu vaut *champ*.

 1.1. Si un label est trouvé, son attribut **for** est alors utilisé pour rechercher un élément dont l'**id** vaut celui trouvé.

2. Si aucun **label** correspondant n'est trouvé, une nouvelle recherche est faite sur tous les éléments dont l'**id** vaut *champ*.
3. Si aucun élément n'est trouvé, une nouvelle recherche est faite sur tous les éléments dont le **name** vaut *champ*.
4. Si aucun élément n'est trouvé, une nouvelle recherche est faite sur tous les éléments dont le **title** vaut *champ*.


Vérification de la présence d'un bouton
---------------------------------------

Vocabulaire
~~~~~~~~~~~

.. code-block:: gherkin

    la page contient un bouton "{valeur}"

Exemples
~~~~~~~~

.. code-block:: gherkin

    Alors la page contient un bouton "Retour"

Quand l'utiliser
~~~~~~~~~~~~~~~~

Cette étape permet de vérifier la présence d'un bouton dans la page.


Vérification de la présence d'un lien
-------------------------------------

Vocabulaire
~~~~~~~~~~~

.. code-block:: gherkin

    la page contient un lien "{valeur}"
    la page contient un lien "{valeur}" qui pointe vers "{target}"

Exemples
~~~~~~~~

.. code-block:: gherkin

    Alors la page contient un lien "Retour"
    Et la page contient un lien "Retour" qui pointe vers "https://back.org"

Quand l'utiliser
~~~~~~~~~~~~~~~~

Cette étape permet de vérifier la présence d'un lien dans la page.


Vérification de la présence d'une image
---------------------------------------

Vocabulaire
~~~~~~~~~~~

.. code-block:: gherkin

    la page contient une image "{valeur}"

Exemples
~~~~~~~~

.. code-block:: gherkin

    Alors la page contient une image "logo"
    Et la page contient une image "https://example.org/logo.png"

Quand l'utiliser
~~~~~~~~~~~~~~~~

Cette étape permet de vérifier la présence d'une image dans la page.


Vérification de la valeur d'une variable
----------------------------------------

Vocabulaire
~~~~~~~~~~~

.. code-block:: gherkin

    la variable "{variable}" vaut "{valeur}"
    la variable "{variable_a}" est égale à la variable "{variable_b}"
    la variable "{variable_a}" est différente de la variable "{variable_b}"
    la variable "{variable_a}" est inférieure à la variable "{variable_b}"
    la variable "{variable_a}" est supérieure à la variable "{variable_b}"

Exemples
~~~~~~~~

.. code-block:: gherkin

    Alors la variable "foo" vaut "bar"

Quand l'utiliser
~~~~~~~~~~~~~~~~

Cette étape permet de vérifier la valeur d'une variable.


Vérification du titre d'un champ
--------------------------------

Vocabulaire
~~~~~~~~~~~

.. code-block:: gherkin

    le titre du champ "{champ}" est "{texte}"

Exemples
~~~~~~~~

.. code-block:: gherkin

    Alors le titr edu champ "foo" est "bar"

Quand l'utiliser
~~~~~~~~~~~~~~~~

Cette étape permet de vérifier la valeur du titre d'un champ.


Vérification du contenu d'un élément
------------------------------------

Vocabulaire
~~~~~~~~~~~

.. code-block:: gherkin

    le champ "{champ}" est vide
    le champ "{champ}" contient "{texte}"
    le champ "{champ}" ne contient pas "{texte}"
    le champ "{champ}" n'est pas vide
    le champ "{champ}" contient "{nombre}" caractères
    le champ "{champ}" contient moins de "{nombre}" caractères
    le champ "{champ}" contient plus de "{nombre}" caractères
    le champ "{champ}" contient entre "{nombre*min}" et "{nombre*max}" caractères

Exemples
~~~~~~~~

.. code-block:: gherkin

    Alors le champ "Adresse" est vide
    Et le champ "Code postal" contient "49000"
    Et le champ "Code postal" ne contient pas "51000"
    Et le champ "Ville" n'est pas vide
    Et le champ "SIRET" contient "12" caractères
    Et le champ "Informations complémentaires" contient moins de "120" caractères
    Et le champ "Password" contient plus de "12" caractères
    Et le champ "Code postal" contient entre "5" et "10" caractères

Quand l'utiliser
~~~~~~~~~~~~~~~~

Cette étape permet de vérifier le contenu de l'attribut *value* d'un champ de formulaire avec plusieurs possibilités.

Subtilités de sélection
~~~~~~~~~~~~~~~~~~~~~~~

1. En premier lieu, la recherche se fait pour les tags **label** dont le contenu vaut *champ*.

 1.1. Si un label est trouvé, son attribut **for** est alors utilisé pour rechercher un élément dont l'**id** vaut celui trouvé.

2. Si aucun **label** correspondant n'est trouvé, une nouvelle recherche est faite sur tous les éléments dont l'**id** vaut *champ*.
3. Si aucun élément n'est trouvé, une nouvelle recherche est faite sur tous les éléments dont le **name** vaut *champ*.
4. Si aucun élément n'est trouvé, une nouvelle recherche est faite sur tous les éléments dont le **title** vaut *champ*.


Vérification des options d'une liste de choix
---------------------------------------------

Vocabulaire
~~~~~~~~~~~

.. code-block:: gherkin

    le champ "{champ}" contient l'option "{valeur}"
    le champ "{champ}" contient l'option ""
    le champ "{champ}" ne contient pas l'option "{valeur}"
    le champ "{champ}" ne contient pas l'option ""
    le champ "{champ}" contient "{valeur}" options
    l'option "{valeur}" du champ "{champ}" est sélectionnée
    l'option "" du champ "{champ}" est sélectionnée

Exemples
~~~~~~~~

.. code-block:: gherkin

    Alors le champ "Logement" contient l'option "T1"
    Et l'option "F4" du champ "Logement" est sélectionnée

Quand l'utiliser
~~~~~~~~~~~~~~~~

Cette étape permet de vérifier la liste des options d'une liste de choix. Elle permet également de vérifier quelle option est sélectionnée.

Subtilités de sélection
~~~~~~~~~~~~~~~~~~~~~~~

1. En premier lieu, la recherche se fait pour les tags **label** dont le contenu vaut *champ*.

 1.1. Si un label est trouvé, son attribut **for** est alors utilisé pour rechercher un **select** dont l'**id** vaut celui trouvé, puis une option dont le texte vaut *valeur*.

2. Si aucun **label** correspondant n'est trouvé, une nouvelle recherche est faite en se basant sur l'**id** des tags **select**, puis sur une option dont le texte vaut *valeur*.
3. Si aucun élément n'est trouvé, une nouvelle recherche est faite en se basant  sur l'**id** des tags **select**, puis sur une option dont le **title** vaut *valeur*.
4. Si aucun **label** correspondant n'est trouvé, une nouvelle recherche est faite en se basant sur le **name** des tags **select**, puis sur une option dont le texte vaut *valeur*.
5. Si aucun élément n'est trouvé, une nouvelle recherche est faite en se basant  sur le **name** des tags **select**, puis sur une option dont le **title** vaut *valeur*.
6. Dans le cas où aucun **select** correspondant n'est trouvé, la recherche s'oriente alors vers les cases à cocher
7. Une nouvelle recherche est faite en se basant sur le texte contenu des **input** de type *radio*, qui doit être égal à *valeur*.
8. Si aucun élément n'est trouvé, une nouvelle recherche est faite en se basant sur le texte contenu des **input** de type *radio* dont l'**id** vaut *champ*, qui doit être égal à *valeur*.
9. Si aucun élément n'est trouvé, une nouvelle recherche est faite en se basant sur le texte contenu des **input** de type *radio* dont le **name** vaut *champ*, qui doit être égal à *valeur*.
10. Si aucun élément n'est trouvé, une nouvelle recherche est faite en se basant sur les **input** de type *radio* dont l'**id** vaut *champ* et dont le **value** doit être égal à *valeur*.
11. Si aucun élément n'est trouvé, une nouvelle recherche est faite en se basant sur les **input** de type *radio* dont le **name** vaut *champ* et dont le **value** doit être égal à *valeur*.


Vérification de la sélection d'une case à cocher
------------------------------------------------

Vocabulaire
~~~~~~~~~~~

.. code-block:: gherkin

    la case à cocher "{champ}" est cochée
    la case à cocher "{champ}" n'est pas cochée
    la case à cocher "{champ}" est décochée

Exemples
~~~~~~~~

.. code-block:: gherkin

    Alors la case à cocher "Conservez mes informations" est cochée
    Et la case à cocher "Se souvenir de moi" n'est pas cochée

Quand l'utiliser
~~~~~~~~~~~~~~~~

Cette étape permet de vérifier si une case à cocher est cochée ou décochée.

Subtilités de sélection
~~~~~~~~~~~~~~~~~~~~~~~

1. La recherche est faite en se basant sur le texte contenu des **input** de type *checkbox*, qui doit être égal à *valeur*.
2. Si aucun élément n'est trouvé, une nouvelle recherche est faite en se basant sur le texte contenu des **input** de type *checkbox* dont l'**id** vaut *champ*, qui doit être égal à *valeur*.
3. Si aucun élément n'est trouvé, une nouvelle recherche est faite en se basant sur le texte contenu des **input** de type *checkbox* dont le **name** vaut *champ*, qui doit être égal à *valeur*.
4. Si aucun élément n'est trouvé, une nouvelle recherche est faite en se basant sur les **input** de type *checkbox* dont l'**id** vaut *champ* et dont le **value** doit être égal à *valeur*.
5. Si aucun élément n'est trouvé, une nouvelle recherche est faite en se basant sur les **input** de type *checkbox* dont le **name** vaut *champ* et dont le **value** doit être égal à *valeur*.


Cliquer sur un lien ou un bouton
--------------------------------

Vocabulaire
~~~~~~~~~~~

.. code-block:: gherkin

    je clique sur le bouton "{valeur}"
    je clique sur le lien "{valeur}"


Exemples
~~~~~~~~

.. code-block:: gherkin

    Alors je clique sur le bouton "Valider"
    Et je clique sur le lien "Retour"

Quand l'utiliser
~~~~~~~~~~~~~~~~

Cette étape permet de vérifier l'existence d'un élément spécifique.


Vérification de l'existence d'un élément
----------------------------------------

Vocabulaire
~~~~~~~~~~~

.. code-block:: gherkin

    un élément contenant "{valeur}" existe

Exemples
~~~~~~~~

.. code-block:: gherkin

    Alors un élément contenant "Demande validée" existe

Quand l'utiliser
~~~~~~~~~~~~~~~~

Cette étape permet de vérifier l'existence d'un élément spécifique.


Vérification de l'existence d'un champ de formulaire
----------------------------------------------------

Vocabulaire
~~~~~~~~~~~

.. code-block:: gherkin

    le champ "{champ}" existe
    le champ "{champ}" n'existe pas

Exemples
~~~~~~~~

.. code-block:: gherkin

    Alors le champ "Deuxième enfant" existe
    Et le champ "Quinzième enfant" n'existe pas

Quand l'utiliser
~~~~~~~~~~~~~~~~

Cette étape permet de vérifier si un champ de formulaire est présent ou pas.

Subtilités de sélection
~~~~~~~~~~~~~~~~~~~~~~~

1. En premier lieu, la recherche se fait pour les tags **label** dont le contenu vaut *champ*.

 1.1. Si un label est trouvé, son attribut **for** est alors utilisé pour rechercher un élément dont l'**id** vaut celui trouvé.

2. Si aucun **label** correspondant n'est trouvé, une nouvelle recherche est faite sur tous les éléments dont l'**id** vaut *champ*.
3. Si aucun élément n'est trouvé, une nouvelle recherche est faite sur tous les éléments dont le **name** vaut *champ*.
4. Si aucun élément n'est trouvé, une nouvelle recherche est faite sur tous les éléments dont le **title** vaut *champ*.


Vérification sur les tableaux
-----------------------------

Vocabulaire
~~~~~~~~~~~

.. code-block:: gherkin

    le tableau contient "{nombre}" colonnes
    le tableau contient "{nombre}" lignes
    le tableau contient moins de "{nombre}" lignes
    la ligne "{nombre}" de la colonne "{colonne}" du tableau contient "{valeur}"
    la ligne "{nombre}" de la colonne "{colonne}" du tableau contient ""
    la ligne "{nombre}" de la colonne "{colonne}" du tableau est vide
    la ligne "{nombre}" de la colonne "{colonne}" du tableau n'est pas vide
    la colonne "{colonne}" du tableau est triée dans l'ordre croissant
    la colonne "{colonne}" du tableau est triée dans l'ordre décroissant
    la colonne "{colonne}" du tableau est triée dans l'ordre alphabétique
    la colonne "{colonne}" du tableau est triée dans l'ordre alphabétique inversé

Exemples
~~~~~~~~

.. code-block:: gherkin

    Alors le tableau contient "6" colonnes
    Et le tableau contient "10" lignes
    Et le tableau contient moins de "21" lignes
    Et la ligne "3" de la colonne "Code postal" du tableau contient "75000"
    Et la ligne "3" de la colonne "Code postal" du tableau contient ""
    Et la ligne "3" de la colonne "Code postal" du tableau n'est pas vide
    Et la ligne "3" de la colonne "Code postal" du tableau est vide
    Et la colonne "Commune" du tableau est triée dans l'ordre croissant
    Et la colonne "Commune" du tableau est triée dans l'ordre alphabétique
    Et la colonne "Commune" du tableau est triée dans l'ordre décroissant
    Et la colonne "Commune" du tableau est triée dans l'ordre alphabétique inversé

Quand l'utiliser
~~~~~~~~~~~~~~~~

Cette étape permet de vérifier plusieurs choses sur un tableau de données.

Subtilités de sélection
~~~~~~~~~~~~~~~~~~~~~~~

1. La recherche est faite en se basant sur le premier élément **table** de la page.
2. La recherche des colonnes se base d'abord sur les éléments **th** de l'élément **thead**.

 2.1. Si aucun élément **thead** n'est trouvé, alos les **th**, puis les premiers **td** sont utilisés.

3. La recherche des lignes se base sur les éléments **td** de chaque élément **tr**.


Vérification de présence d'un bouton
------------------------------------

Vocabulaire
~~~~~~~~~~~

.. code-block:: gherkin

    le bouton "{bouton}" existe
    le bouton "{bouton}" n'existe pas

Exemples
~~~~~~~~

.. code-block:: gherkin

    Alors le bouton "Créer une demande" existe
    Et le bouton "Supprimer une demande" n'existe pas

Quand l'utiliser
~~~~~~~~~~~~~~~~

Cette étape permet de vérifier la présence ou l'absence d'un bouton sur la page.

Subtilités de sélection
~~~~~~~~~~~~~~~~~~~~~~~

1. En premier lieu, tous les éléments dont l'**id** vaut *valeur* sont sélectionnés.
2. Si aucun élément n'est trouvé, une nouvelle recherche se fait en se basant sur le **name** des éléments.
3. Si aucun élément n'est trouvé, une nouvelle recherche est faite en se basant sur le **title** des éléments de type *button*, *input*, *img* ou *a* uniquement.
4. Si aucun élément n'est trouvé, une nouvelle recherche est faite en se basant sur la **value** des éléments de type *button*, *input*, *img* ou *a* uniquement.
5. Si aucun élément n'est trouvé, une nouvelle recherche est faite en se basant sur le texte contenu des éléments de type *button*, *input*, *img* ou *a* uniquement.
