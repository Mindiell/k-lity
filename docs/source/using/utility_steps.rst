.. _utility_steps:

Etapes utilitaires
==================

Ces étapes sont utilisables à tout moment des tests.

Attente
-------

Vocabulaire
~~~~~~~~~~~

.. code-block:: gherkin

    j'attends {secondes} secondes
    que j'attends {secondes} secondes
    j'attends {seconde} seconde
    que j'attends {seconde} seconde

Exemples
~~~~~~~~

.. code-block:: gherkin

    Etant donné que j'attends 5 secondes
    Quand j'attends 3 secondes
    Alors j'attends 1 secondes

Quand l'utiliser
~~~~~~~~~~~~~~~~

Cette étape permet d'attendre un certain temps, par exemple lorsqu'on fait les tests
manuellement et qu'on a besoin de vérifier que les étapes précédentes se sont bien
déroulées. Il n'est, en général, pas nécessaire de conserver ces étapes pour les tests
finaux.


Faire une capture d'écran
-------------------------

Vocabulaire
~~~~~~~~~~~

.. code-block:: gherkin

    je fais un screenshot
    que je fais un screenshot
    je fais un screenshot sous le nom "{nom_fichier}"
    que je fais un screenshot sous le nom "{nom_fichier}"
    je fais une capture d'écran
    que je fais une capture d'écran
    je fais une capture d'écran sous le nom "{nom_fichier}"
    que je fais une capture d'écran sous le nom "{nom_fichier}"


Exemples
~~~~~~~~

.. code-block:: gherkin

    Etant donné que je fais une capture d'écran
    Quand je fais un screenshot sous le nom "page d'accueil"
    Alors je fais une capture d'écran sous le nom "résultat_$identifiant$"

Quand l'utiliser
~~~~~~~~~~~~~~~~

Cette étape permet de faire une capture de l'écran actuel et de sauver celle-ci dans le
répertoire **screenshots** prévu à cet effet. Par défaut, chaque capture d'écran est
sauvée avec la date et l'heure de la capture en tant que nom de fichier.

Comme indiqué dans le dernier exemple, il est tout à fait possible d'utiliser une
:ref:`variable<variables>` dans le nom du fichier.


Assigner une valeur à une variable
----------------------------------

Vocabulaire
~~~~~~~~~~~

.. code-block:: gherkin

    j'assigne la valeur "{valeur}" à la variable "{variable}"
    que j'assigne la valeur "{valeur}" à la variable "{variable}"
    j'assigne la valeur du champ "{champ}" à la variable "{variable}"
    que j'assigne la valeur du champ "{champ}" à la variable "{variable}"


Exemples
~~~~~~~~

.. code-block:: gherkin

    Etant donné que j'assigne la valeur "toto" à la variable "prénom"
    Quand j'assigne la valeur "$mail$" à la variable "identifiant"
    Alors j'assigne la valeur du champ "Nom" à la variable "nom"

Quand l'utiliser
~~~~~~~~~~~~~~~~

Cette étape permet d'assigner une valeur à une variable. On notera qu'il ne faut pas
entourer le nom de la variable ciblée de caractères "$" sous peine de la voir remplacer
par sa valeur actuelle, ce qui n'est peut-être pas le but recherché, mais cela reste
possible. Voir :ref:`variables` pour plus d'infomations.


Exécuter une requête
--------------------

Vocabulaire
~~~~~~~~~~~

.. code-block:: gherkin

j'exécute la requête "{requête}"
que j'exécute la requête "{requête}"
j'exécute la requête "{requête}" avec le paramètre "{paramètre}"
que j'exécute la requête "{requête}" avec le paramètre "{paramètre}"
j'exécute la requête "{requête}" avec les paramètres "{paramètres}"
que j'exécute la requête "{requête}" avec les paramètres "{paramètres}"


Exemples
~~~~~~~~

.. code-block:: gherkin

    Etant donné que j'exécute la requête "première_requête"
    Quand j'exécute la requête "seconde_requête" avec le paramètre "$param$"
    Alors j'exécute la requête "dernière_requête" avec les paramètres "toto,titi"

Quand l'utiliser
~~~~~~~~~~~~~~~~

Cette étape permet d'exécuter une requête SQL à travers un connecteur spécifique. Si
plusieurs paramètre sont fournis, ils sont séparés par des virgules.

Comme indiqué dans le deuxième exemple, il est tout à fait possible d'utiliser une
:ref:`variable<variables>` dans le nom de la requête ou des paramètres.


Assigner le résultat d'une requête à une variable
-------------------------------------------------

Vocabulaire
~~~~~~~~~~~

.. code-block:: gherkin

    j'assigne le résultat de la requête "{requête}" à la variable "{variable}"
    que j'assigne le résultat de la requête "{requête}" à la variable "{variable}"
    j'assigne le résultat de la requête "{requête}" avec le paramètre "{paramètre}" à la variable "{variable}"
    que j'assigne le résultat de la requête "{requête}" avec le paramètre "{paramètre}" à la variable "{variable}"
    j'assigne le résultat de la requête "{requête}" avec les paramètres "{paramètres}" à la variable "{variable}"
    que j'assigne le résultat de la requête "{requête}" avec les paramètres "{paramètres}" à la variable "{variable}"


Exemples
~~~~~~~~

.. code-block:: gherkin

    Etant donné que j'assigne le résultat de la requête "première_requête" à la variable "un"
    Quand j'assigne le résultat de la requête "seconde_requête" avec le paramètre "$param$" à la variable "deux"
    Alors j'assigne le résultat de la requête "dernière_requête" avec les paramètres "toto,titi" à la variable "trois"

Quand l'utiliser
~~~~~~~~~~~~~~~~

Cette étape est identique à la précédente. Elle ajoute en plus la possibilité de récupérer
la première valeur de la première colonne pour l'assigner à une variable.


Vérifier qu'une requête renvoie une valeur spécifique
-----------------------------------------------------

Vocabulaire
~~~~~~~~~~~

.. code-block:: gherkin

    le résultat de la requête "{requête}" est vide
    que le résultat de la requête "{requête}" est vide
    le résultat de la requête "{requête}" avec le paramètre "{paramètre}" est vide
    que le résultat de la requête "{requête}" avec le paramètre "{paramètre}" est vide
    le résultat de la requête "{requête}" avec les paramètres "{paramètres}" est vide
    que le résultat de la requête "{requête}" avec les paramètres "{paramètres}" est vide
    le résultat de la requête "{requête}" contient "{texte}"
    que le résultat de la requête "{requête}" contient "{texte}"
    le résultat de la requête "{requête}" avec le paramètre "{paramètre}" contient "{texte}"
    que le résultat de la requête "{requête}" avec le paramètre "{paramètre}" contient "{texte}"
    le résultat de la requête "{requête}" avec les paramètres "{paramètres}" contient "{texte}"
    que le résultat de la requête "{requête}" avec les paramètres "{paramètres}" contient "{texte}"


Exemples
~~~~~~~~

.. code-block:: gherkin

    Alors le résultat de la requête "cherche_utilisateur" est vide
    Alors le résultat de la requête "cherche_utilisateur" contient "$identifiant$"

Quand l'utiliser
~~~~~~~~~~~~~~~~

Cette étape permet de valider la valeur de la première colonne du premier enregistrement
retourné par la requête.


Vérifier qu'une requête renvoie quelque chose
---------------------------------------------

Vocabulaire
~~~~~~~~~~~

.. code-block:: gherkin

    le résultat de la requête "{requête}" n'est pas vide
    que le résultat de la requête "{requête}" n'est pas vide
    le résultat de la requête "{requête}" avec le paramètre "{paramètre}" n'est pas vide
    que le résultat de la requête "{requête}" avec le paramètre "{paramètre}" n'est pas vide
    le résultat de la requête "{requête}" avec les paramètres "{paramètres}" n'est pas vide
    que le résultat de la requête "{requête}" avec les paramètres "{paramètres}" n'est pas vide


Exemples
~~~~~~~~

.. code-block:: gherkin

    Alors le résultat de la requête "dernière_requête" n'est pas vide

Quand l'utiliser
~~~~~~~~~~~~~~~~

Cette étape permet de valider que la première colonne du premier enregistrement retourné
par la requête n'est pas vide.
