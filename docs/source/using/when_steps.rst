.. _when_steps:

Les étapes de type 'Quand'
==========================

Cliquer sur un bouton
---------------------

Vocabulaire
~~~~~~~~~~~

.. code-block:: gherkin

    je clique sur le bouton "{valeur}"
    que je clique sur le bouton "{valeur}"

Exemples
~~~~~~~~

.. code-block:: gherkin

    Quand je clique sur le bouton "Enregistrer"
    Et que je clique sur le bouton "Sauver"

Quand l'utiliser
~~~~~~~~~~~~~~~~

Le cas le plus simple reste le clic sur un bouton, mais parfois les images ou les liens
sont représentés sous la forme de bouton. Dans ce cas, il est aussi simple d'utiliser
cette étape.

Subtilités de sélection
~~~~~~~~~~~~~~~~~~~~~~~

1. En premier lieu, tous les éléments dont l'**id** vaut *valeur* sont sélectionnés.
2. Si aucun élément n'est trouvé, une nouvelle recherche se fait en se basant sur le **name** des éléments.
3. Si aucun élément n'est trouvé, une nouvelle recherche est faite en se basant sur le **title** des éléments de type *button*, *input*, *img* ou *a* uniquement.
4. Si aucun élément n'est trouvé, une nouvelle recherche est faite en se basant sur la **value** des éléments de type *button*, *input*, *img* ou *a* uniquement.
5. Si aucun élément n'est trouvé, une nouvelle recherche est faite en se basant sur le texte contenu des éléments de type *button*, *input*, *img* ou *a* uniquement.


Cliquer sur un lien
-------------------

Vocabulaire
~~~~~~~~~~~

.. code-block:: gherkin

    je clique sur le lien "{valeur}"
    que je clique sur le lien "{valeur}"

Exemples
~~~~~~~~

.. code-block:: gherkin

    Quand je clique sur le lien "Enregistrer"
    Et que je cliquer sur le lien "Sauver"

Quand l'utiliser
~~~~~~~~~~~~~~~~

Contrairement à l'étape précédente, celle-ci ne vise que les liens.

Subtilités de sélection
~~~~~~~~~~~~~~~~~~~~~~~

1. En premier lieu, tous les **a** dont l'**id** vaut *valeur* sont sélectionnés.
2. Si aucun élément n'est trouvé, une nouvelle recherche se fait en se basant sur le **name** des **a**.
3. Si aucun élément n'est trouvé, une nouvelle recherche se fait en se basant sur le **title** des **a**.
4. Si aucun élément n'est trouvé, une nouvelle recherche se fait en se basant sur le texte contenu des **a**.


Cliquer sur un élément
----------------------

Vocabulaire
~~~~~~~~~~~

.. code-block:: gherkin

    je clique sur l'élément contenant "{valeur}"
    que je clique sur l'élément contenant "{valeur}"

Exemples
~~~~~~~~

.. code-block:: gherkin

    Quand je clique sur l'élément contenant "49000 - Angers"
    Et que je cliquer sur l'élément contenant "35000 - Rennes"

Quand l'utiliser
~~~~~~~~~~~~~~~~

Cette étape permet de déclencher des actions qui ne sont pas déclenchées naturellement en HTML. C'es ttypiquement le cas pour des actions javascript.

Subtilités de sélection
~~~~~~~~~~~~~~~~~~~~~~~

1. En premier lieu, tous les éléments sauf **p** et **div** dont le texte contenu vaut *valeur* sont sélectionnés.
2. Si aucun élément n'est trouvé, une nouvelle recherche se fait en se basant sur l'**id** de n'importe quel élément.
3. Si aucun élément n'est trouvé, une nouvelle recherche se fait en se basant sur le **name** de n'importe quel élément.
4. Si aucun élément n'est trouvé, une nouvelle recherche se fait en se basant sur le **value** de n'importe quel élément.
5. Si aucun élément n'est trouvé, une nouvelle recherche se fait en se basant sur le texte contenu de n'importe quel élément.


Sélection d'une option dans une liste de choix
----------------------------------------------

Vocabulaire
~~~~~~~~~~~

.. code-block:: gherkin

    je sélectionne la valeur "{valeur}" du champ "{champ}"
    que je sélectionne la valeur "{valeur}" du champ "{champ}"

Exemples
~~~~~~~~

.. code-block:: gherkin

    Quand je sélectionne la valeur "Oui" du champ "Voulez-vous"
    Et que je sélectionne la valeur "Bleu" du champ "Couleur"

Quand l'utiliser
~~~~~~~~~~~~~~~~

Cette étape permet de sélectionner une valeur précise dans un champ de type sélection (select ou bouton radio) dans un formulaire.

Subtilités de sélection
~~~~~~~~~~~~~~~~~~~~~~~

1. En premier lieu, la recherche se fait pour les tags **label** dont le contenu vaut *champ*.

 1.1. Si un label est trouvé, son attribut **for** est alors utilisé pour rechercher un **select** dont l'**id** vaut celui trouvé, puis une option dont le texte vaut *valeur*.

2. Si aucun **label** correspondant n'est trouvé, une nouvelle recherche est faite en se basant sur l'**id** des tags **select**, puis sur une option dont le texte vaut *valeur*.
3. Si aucun élément n'est trouvé, une nouvelle recherche est faite en se basant  sur l'**id** des tags **select**, puis sur une option dont le **title** vaut *valeur*.
4. Si aucun **label** correspondant n'est trouvé, une nouvelle recherche est faite en se basant sur le **name** des tags **select**, puis sur une option dont le texte vaut *valeur*.
5. Si aucun élément n'est trouvé, une nouvelle recherche est faite en se basant  sur le **name** des tags **select**, puis sur une option dont le **title** vaut *valeur*.
6. Dans le cas où aucun **select** correspondant n'est trouvé, la recherche s'oriente alors vers les cases à cocher
7. Une nouvelle recherche est faite en se basant sur le texte contenu des **input** de type *radio*, qui doit être égal à *valeur*.
8. Si aucun élément n'est trouvé, une nouvelle recherche est faite en se basant sur le texte contenu des **input** de type *radio* dont l'**id** vaut *champ*, qui doit être égal à *valeur*.
9. Si aucun élément n'est trouvé, une nouvelle recherche est faite en se basant sur le texte contenu des **input** de type *radio* dont le **name** vaut *champ*, qui doit être égal à *valeur*.
10. Si aucun élément n'est trouvé, une nouvelle recherche est faite en se basant sur les **input** de type *radio* dont l'**id** vaut *champ* et dont le **value** doit être égal à *valeur*.
11. Si aucun élément n'est trouvé, une nouvelle recherche est faite en se basant sur les **input** de type *radio* dont le **name** vaut *champ* et dont le **value** doit être égal à *valeur*.


Saisie d'une valeur dans un champ de formulaire
-----------------------------------------------

Vocabulaire
~~~~~~~~~~~

.. code-block:: gherkin

    je tape "{valeur}" dans le champ "{champ}"
    que je tape "{valeur}" dans le champ "{champ}"
    je tape "" dans le champ "{champ}"
    que je tape "" dans le champ "{champ}"
    je vide le champ "{champ}"
    que je vide le champ "{champ}"
    je tape ""
    que je tape ""
    je tape "{valeur}"
    que je tape "{valeur}"

Exemples
~~~~~~~~

.. code-block:: gherkin

    Quand je tape "1, rue Jean Jaurès" dans le champ "Adresse"
    Et que je tape "Paris" dans le champ "Ville"
    Et que je tape "" dans le champ "Pays"
    Et que je vide le champ "Téléphone"
    Et que je tape "01" dans le champ "Début de validité"
    Et que je tape "08"
    Et que je tape "2010"

Quand l'utiliser
~~~~~~~~~~~~~~~~

Ces étapes permettent de définir des valeurs pour des champs de formulaire. Chacune peut être utilisée suivant les besoins du test. Il faut noter que les étapes 'je tape "{valeur}"' tape dans le navigateur sans pointer un champ spécifique.

Subtilités de sélection
~~~~~~~~~~~~~~~~~~~~~~~

1. En premier lieu, la recherche se fait pour les tags **label** dont le contenu vaut *champ*.

 1.1. Si un label est trouvé, son attribut **for** est alors utilisé pour rechercher un élément dont l'**id** vaut celui trouvé.

2. Si aucun **label** correspondant n'est trouvé, une nouvelle recherche est faite sur tous les éléments dont l'**id** vaut *champ*.
3. Si aucun élément n'est trouvé, une nouvelle recherche est faite sur tous les éléments dont le **name** vaut *champ*.
4. Si aucun élément n'est trouvé, une nouvelle recherche est faite sur tous les éléments dont le **title** vaut *champ*.


Sélectionner un bouton radio
----------------------------

Vocabulaire
~~~~~~~~~~~

.. code-block:: gherkin

    je clique sur le bouton radio "{valeur}"
    que je clique sur le bouton radio "{valeur}"

Exemples
~~~~~~~~

.. code-block:: gherkin

    Quand je clique sur le bouton radio "Oui"

Quand l'utiliser
~~~~~~~~~~~~~~~~

Ces étapes permettent de sélectionner la valeur d'un bouton radio (**input** de type *radio*).


Cocher ou décocher une case à cocher
------------------------------------

Vocabulaire
~~~~~~~~~~~

.. code-block:: gherkin

    je coche la case à cocher "{valeur}"
    que je coche la case à cocher "{valeur}"
    je décoche la case à cocher "{valeur}"
    que je décoche la case à cocher "{valeur}"

Exemples
~~~~~~~~

.. code-block:: gherkin

    Quand je coche la case à cocher "Oui"
    Et que je décoche la case à cocher "Recevoir des notifications"

Quand l'utiliser
~~~~~~~~~~~~~~~~

Ces étapes permettent de cocher ou décocher des cases à cocher (**input** de type *checkbox*).

Subtilités de sélection
~~~~~~~~~~~~~~~~~~~~~~~

1. La recherche est faite en se basant sur le texte contenu des **input** de type *checkbox*, qui doit être égal à *valeur*.
2. Si aucun élément n'est trouvé, une nouvelle recherche est faite en se basant sur le texte contenu des **input** de type *checkbox* dont l'**id** vaut *champ*, qui doit être égal à *valeur*.
3. Si aucun élément n'est trouvé, une nouvelle recherche est faite en se basant sur le texte contenu des **input** de type *checkbox* dont le **name** vaut *champ*, qui doit être égal à *valeur*.
4. Si aucun élément n'est trouvé, une nouvelle recherche est faite en se basant sur les **input** de type *checkbox* dont l'**id** vaut *champ* et dont le **value** doit être égal à *valeur*.
5. Si aucun élément n'est trouvé, une nouvelle recherche est faite en se basant sur les **input** de type *checkbox* dont le **name** vaut *champ* et dont le **value** doit être égal à *valeur*.


Sélectionner un fichier à transmettre
-------------------------------------

Vocabulaire
~~~~~~~~~~~

.. code-block:: gherkin

    que je sélectionne le fichier "{fichier}" dans le champ "{champ}"

Exemples
~~~~~~~~

.. code-block:: gherkin

    Quand je sélectionne le fichier "pièce jointe.pdf" dans le champ "Ajouter une pièce jointe"

Quand l'utiliser
~~~~~~~~~~~~~~~~

Ces étapes permettent de fournir un fichier au formulaire afin de transmettre le fichier au serveur.
Le fichier doit être situé dans le répertoire où se déroulent les tests. Il est généralement intéressant de laisser les fichiers liés à un test à côté du fichier *feature* qui lui correspond.

Subtilités de sélection
~~~~~~~~~~~~~~~~~~~~~~~

1. En premier lieu, la recherche se fait pour les tags **label** dont le contenu vaut *champ*.

 1.1. Si un label est trouvé, son attribut **for** est alors utilisé pour rechercher un élément dont l'**id** vaut celui trouvé.

2. Si aucun **label** correspondant n'est trouvé, une nouvelle recherche est faite sur tous les éléments dont l'**id** vaut *champ*.
3. Si aucun élément n'est trouvé, une nouvelle recherche est faite sur tous les éléments dont le **name** vaut *champ*.
4. Si aucun élément n'est trouvé, une nouvelle recherche est faite sur tous les éléments dont le **title** vaut *champ*.


Attendre l'apparition d'un élément
----------------------------------

Vocabulaire
~~~~~~~~~~~

.. code-block:: gherkin

    j'attends un élément contenant "{valeur}"
    que j'attends un élément contenant "{valeur}"

Exemples
~~~~~~~~

.. code-block:: gherkin

    Quand j'attends un élément contenant "Nouveaux échanges"

Quand l'utiliser
~~~~~~~~~~~~~~~~

Cette étape est spécifique et concerne généralement des actions Ajax. En effet, il est nécessaire d'attendre que le navigateur ait reçu et pris en compte la réponse. Le test attend alors l'affichage d'un contenu spécifique. Cette étape est généralement utilisée avant de cliquer sur l'élement.

Subtilités de sélection
~~~~~~~~~~~~~~~~~~~~~~~

Le test va tenter de trouver l'élement toutes les 0,1 seconde.

1. En premier lieu, tous les éléments sauf **p** et **div** dont le texte contenu vaut *valeur* sont sélectionnés.
2. Si aucun élément n'est trouvé, une nouvelle recherche se fait en se basant sur l'**id** de n'importe quel élément.
3. Si aucun élément n'est trouvé, une nouvelle recherche se fait en se basant sur le **name** de n'importe quel élément.
4. Si aucun élément n'est trouvé, une nouvelle recherche se fait en se basant sur le **value** de n'importe quel élément.
5. Si aucun élément n'est trouvé, une nouvelle recherche se fait en se basant sur le texte contenu de n'importe quel élément.


Cliquer sur le ReCaptcha
------------------------

Vocabulaire
~~~~~~~~~~~

.. code-block:: gherkin

    je clique sur le ReCaptcha
    que je clique sur le ReCaptcha

Exemples
~~~~~~~~

.. code-block:: gherkin

    Quand je clique sur le ReCaptcha

Quand l'utiliser
~~~~~~~~~~~~~~~~

Cette étape est spécifique et concerne uniquement le ReCaptcha de test. Elle ne pourra absolument pas servir à passer un ReCaptcha
configuré pour protéger un site mais bien un ReCaptcha configuré en mode TEST.
