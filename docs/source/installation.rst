.. _installation:

Installation
============

Pour installer K-lity, il suffit d'utiliser pip :

::

    pip install k-lity

Si vous souhaitez utiliser postgresql lors de vos tests, il est nécessaire de le
signaler lors de l'installation :

::

    pip install k-lity[postgresql]

Pour le moment, seul postgresql est supporté.


Mise à jour
-----------

Pour mettre K-lity à jour, là encore pip est suffisant :

::

    pip install -U k-lity


Geckodriver
-----------

Actuellement, seul Firefox est géré par K-lity. Il est donc nécessaire d'installer un
outil pour piloter celui-ci. C'est l'outil
`geckodriver <https://github.com/mozilla/geckodriver/releases/latest>`_
qui est utilisé ici. Il faut bien entendu ajouter le chemin de l'exécutable au PATH de
l'utilisateur pour le rendre accessible.


Remarques
---------

 * Seul Firefox est utilisé pour le moment
 * Seules les bases postgresql sont utilisables pour le moment



Installation pas à pas sous Windows
-----------------------------------

Installation et configuration de Python
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Il est d'abord nécessaire d'installer une version récente de Python. Commencez par
télécharger `la dernière version de Python disponible
<https://www.python.org/downloads/windows/>`_.

**Attention :** K-lity est compatible avec les version de Python 3.7 et supérieur.


Vous devriez pouvoir maintenant ouvrir une fenêtre de commande et exécuter les commandes
suivantes :

::

    python --version

qui devrait vous renvoyer quelque chose comme :

::

    Python 3.9.6

et

::

    pip --version

qui devrait vous renvoyer quelque chose comme :

::

    pip 18.1.6 from [un chemin] (python3.9)

Quelle que soit la version de *pip*, il est toujours intéressant d'utiliser sa dernière
version. Pour cela, mettez-le à jour :

::

    python -m pip install -U pip


Vous devriez maintenant pouvoir installer et utilier K-lity en reprenant :ref:`les
étapes d'installation<installation>`.
