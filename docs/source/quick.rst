.. _quick_start:

Démarrage rapide
================

Afin de lancer un premier test, vous devez créer un répertoire projet. K-lity
vous aide via la commande *klity-newproject* :

::

    klity-newproject mytest

Vous obtenez alors un répertoire appelé mytest avec cette structure :

::

    mytest
    ├─ configuration.yml
    ├─ environment.py
    └─ steps
       └─ project_steps.py


* *configuration.yml* est un fichier de configuration par défaut. Modifiez-le pour
    spécifier votre propre configuration.
* *environment.py* est un fichier python pour usage interne, n'y touchez pas.
* *steps* est le répertoire dans lequel vous allez écrire tous vos tests.
* *project_steps.py* est un fichier python qui contient les étapes spécifiques à votre
    projet.


Un premier test
---------------

Pour écrire votre test, vous n'avez besoin que d'un éditeur de texte. Créer un
fichier first_test.feature dans le répertoire steps et écrivez le test
suivant:

.. code-block:: gherkin

    Fonctionnalité: Premier test avec K-lity

    Scénario: Un exemple avec example.org
        Etant donné que je visite le site "https://example.org"
        Alors la page contient "Example Domain"

Vous pouvez alors lancer votre test depuis le répertoire parent *my_test* et
voir le test se dérouler à votre écran. Logiquement, vous devriez voir Firefox se lancer
et rapidement se refermer une fois que le site *example.org* est chargé::

    > klity
    first_test
      Executing tests

Maintenant, vous pouvez découvrir qu'un fichier (geckodriver.log) et trois répertoires
ont été créés :

    * *geckodriver.log* contient les logs fournis par geckodriver.
    * *report* contient un rapport sur vos tests.
    * *results* contient les résultats de toutes les commandes behave exécutées pour
        chacune de vos fonctionnalités.Lors de la création des tests, ces fichiers sont
        très importants car ils peuvent vous aider à comprendre ce qui se passe.
    * *screenshots* contient les captures d'écran de vos tests.
