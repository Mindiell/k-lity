.. _configuration:

Configurer K-lity
=================

Le fichier *configuration.yml* permet d'ajuster le comportement de K-lity au plus juste.

behave
------

Cette option permet de configurer *behave* lui-même.

.. code-block:: yaml

    behave:
        lang: fr

**Valeurs par défaut**

La langue est positionnée à "fr".



databases
---------

Cette option permet de configurer une à plusieurs connexions à des bases de données. Il
s'agit d'un dictionnaire d'informations de connexions dont la clef représente le nom de
la connexion à utiliser dans les requêtes SQL.

Actuellement, seules les connexions *postgresql* sont gérées.

.. code-block:: yaml

    databases:
        db_1:
            type: postgresql
            host: localhost
            database: my_db
            user: user
            password: password
        db_2:
            type: postgresql
            host: some_server
            database: other_db
            user: user
            password: complex_password

**Valeurs par défaut**

La liste est vide.



variables
---------

Cette option permet de spécifier des variables globales à tous les tests. Les variables
sont ré-initialisées à chaque test.

.. code-block:: yaml

    variables:
        foo: bar

**Valeurs par défaut**

La liste est vide.



reporting
---------

Cette option permet de configurer les générations de rapport.

.. code-block:: yaml

    reporting:
        title: Some title
        template: some_template


**Valeurs par défaut**

*title* : Default title

*template* : default



options
-------

Cette option permet de configurer le comportement de K-lity.

.. code-block:: yaml

    options:
        timeout: 15
        headless: True


**Valeurs par défaut**

*timeout* : 10

*headless* : False
