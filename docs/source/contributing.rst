Contribuer
==========

K-lity est un projet libre (licence APGL). Tout le monde peut contribuer de bien des
manières :

 * en l'utilisant tout d'abord, et en remontant les éventuels problèmes rencontrés
 * en le testant
 * en aidant à sa documentation
 * en développant également

En cas de besoin, vous pouvez joindre l'équipe via IRC sur `le canal
#k-lity <https://kiwiirc.com/nextclient/irc.geeknode.org/k-lity>`_.
